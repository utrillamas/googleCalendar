const electron = require('electron');
const {app, BrowserWindow} = electron;

app.on('ready', function() {
    let mainWindow = new BrowserWindow({
        titleBarStyle: 'hidden',
        width: 1100,
        height: 1000
    });

    mainWindow.loadURL('file://' + __dirname + '/index.html');
});

app.on('window-all-closed', function() {
    app.quit();
});