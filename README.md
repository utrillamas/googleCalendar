# googleCalendar

Simple google calendar desktop app made with electron

## Install
`npm install -g electron-builder` <br/>
`yarn global add electron-builder`

After that execute this command: 

* **Windows:**<br/>
`build -win --x64`

* **MacOs:**<br/>
`build -mac --x64`

* **Linux:**<br/>
`build --linux --x64`